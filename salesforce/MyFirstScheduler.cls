/**
 * @description This is an example of a schedulable / scheduler class in Apex
 * The execute method is called when this class is scheduled to run.
 * Schedule this class to run by going to Develop->Apex Classes
 * and then click on the 'Schedule Apex' button at the top.
 * This class should show up in the class lookup dialog if everything
 * works correctly.
 * More info on Apex scheduling here: http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_scheduler.htm
 */
global class MyFirstScheduler implements Schedulable{
    global void execute(SchedulableContext sc){
       /*This could be anything really..
         Either call a static method, or
         instanciate a member object and then call one of its methods
         Or kick off a scheduled batch job like below */
       Database.executeBatch(new MyBatchClass());
    }
}
