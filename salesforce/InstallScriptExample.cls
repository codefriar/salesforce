/**
 * InstallScriptExample
 * @description Simple Install Script for Managed Package
 * @author Dancing Llama, Hidden Alpaca
 * @date 5/6/2013
 */
global class InstallScriptExample implements InstallHandler{
    
   /**
     * onInstall
     * @description onInstall method (enforced by InstallHandler interface)
     * for executing install operations on managed package
     */
    global void onInstall(InstallContext ctx){
        
        //Don't insert custom settings and initial data if this is an upgrade...
        if(ctx.isUpgrade){
            return;
        }

        //Insert custom setting data
        insert new My_Custom_Hierachry_Setting__c(
            Bool_Field_1__c=true,
            Bool_Field_2__c=false,
            String_Field_1__c=null,
            SetupOwnerId=UserInfo.getOrganizationId()
        );

        //Insert custom list setting data
        List<My_Custom_List_Setting__c> customListSettings = new List<My_Custom_List_Setting__c>();
        customListSettings.add(new My_Custom_List_Setting__c(Name='name1',Value__c='value1'));
        customListSettings.add(new My_Custom_List_Setting__c(Name='name2',Value__c='value2'));
        insert customListSettings;

        //Insert record type id information.. useful for workflows, validation rules
        //Populate the map below with a map of record type developer name keys,
        //And matching API names of custom setting fields to populate that hierarchy custom setting.
        //Only Hierarhcy custom settings are allowed via workflows and validation rules, hence the custom setting type.
        Map<String,String> recordTypeNameToCustomSettingFieldMap = new Map<String,String>();
        
        Record_Type_Id_Setting__c rtiSetting = new Record_Type_Id_Setting__c(OwnerId=UserInfo.getOrganizationId());
        for(RecordType rt : [Select Id,sObjectType,DeveloperName From RecordType Where DeveloperName in :recordTypeNameToCustomSettingFieldType.values()]){
            for(String devName : recordTypeNameToCustomSettingFieldMap.keySet()){
                if(rt.DeveloperName==devName){
                    rtiSetting.put(recordTypeNameToCustomSettingFieldMap.get(devName),rt.Id);
                }
            }
        }  
    }
}
